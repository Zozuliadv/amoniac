module RoutesAdder
  class CollectsController < ApplicationController

  	skip_before_action :verify_authenticity_token

    def reverse
      result = {}
      param_keys = params.except(:action, :controller)
      param_keys.each do |k, v|
        result[v] = k.reverse
      end
      render :json => result
    end
  end
end