require 'rails_helper'
require "routes_adder"

@controller = RoutesAdder::CollectsController

RSpec.describe @controller, type: :controller do

  describe "PATCH /collect/data" do
    it "returns http success" do
      patch :reverse
      expect(response).to have_http_status(:success)
    end
  end

end